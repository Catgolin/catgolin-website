# Description

Ceci est le code source de mon site personnel.

Si ce code peut vous être de la moindre utilité, vous pouvez l'utiliser en respectant les critères de la licence publique générale GNU (GNU GPL).

## Guide d'utilisation

La version live du site est disponible à l'adresse (https://catgolin.nexgate.ch).

Pour tester le site en local:
1. Télécharger le dépôt `git clone https://framagit.org/Catgolin/catgolin-website`
2. Lancer `composer install`
3. Lancer `symfony server:start`

Pour lancer des tests:
1. [Installer `xdebug`](https://xdebug.org/docs/install)
2. Lancer `composer test`

# Informations diverses

C'est la troisième fois que je reprend le code source de ce site: la tentative précédente est accessible sur le dépôt (https://framagit.org/Catgolin/site);
